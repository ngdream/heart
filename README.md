<p align="center">
<img src="/share/heart.png" width="100">
</p>

# table of contents
- [about the project](about-the-project)
- [stack](stack)
- [contribute](contribute)

# about the project
heart is a c++ web microfrawework easy to use that helps c++ developer to make good websites

# stack
- mongoose
- make
# contribute

all contibutions are welcome heart is made with mongoose for more information about ``how to contribute`` read our contributions guidelines

**hanks for your contributions :fire:**
<p align="center">
<img src="https://forthebadge.com/images/badges/built-with-love.svg">
</p>
